var path = require('path');
var random = require('mongoose-random');
var slug = require('mongoose-url-slugs');
var speakingurl = require('speakingurl');

module.exports = function (mongoose) {

    //Объявляем схему для Mongoose
    var Schema = new mongoose.Schema({
        title: {type: String, required: true},
        content: {type: String, required: true},
        category: {type: String},
        tags: {type: []},
        author: {type: String},
        source: {type: String}
    });
    Schema.plugin(random);
    Schema.plugin(slug('title', {
        maxLength: 50,
        generator: function (text, separator) {
            text = speakingurl(text);

            var slug = text.toLowerCase().replace(/([^a-z0-9\-\_]+)/g, separator).replace(new RegExp(separator + '{2,}', 'g'), separator);
            if (slug.substr(-1) == separator) {
                slug = slug.substr(0, slug.length - 1);
            }

            return slug;
        }
    }));

    // Инициализируем модель с именем файла, в котором она находится
    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};
