var log = require('./libs/log')(module);
var jwt = require('express-jwt');
var secret = require('./config/secret');
var routesUsers = require('./routes/users');

module.exports.setup = function (app, handlers) {
    app.get('/', function (req, res) {
        res.send('Hello World!')
    });

    app.get('/api', function (req, res) {
        res.send('API is running');
    });

    app.post('/api/v1/user/register', routesUsers.register);
    app.post('/api/v1/user/signin', routesUsers.signin);
    app.get('/api/v1/user/logout', jwt({secret: secret.secretToken}), routesUsers.logout);

    app.get('/api/v1/recipes', handlers.recipes.list);
    app.get('/api/v1/recipes/:id', handlers.recipes.get);
    app.post('/api/v1/recipes', handlers.recipes.create);
    app.put('/api/v1/recipes/:id', handlers.recipes.update);
    app.delete('/api/v1/recipes/:id', handlers.recipes.remove);

    app.get('/api/v1/questions',  handlers.questions.list);
    app.get('/api/v1/questions/:slug', handlers.questions.get);
    app.post('/api/v1/questions',  jwt({secret: secret.secretToken}), handlers.questions.create);
    app.put('/api/v1/questions/:slug',  jwt({secret: secret.secretToken}), handlers.questions.update);
    app.delete('/api/v1/questions/:slug',  jwt({secret: secret.secretToken}), handlers.questions.remove);

    app.get('/api/v1/ask',  handlers.ask.list);
    app.get('/api/v1/ask/:slug', handlers.ask.get);
    app.post('/api/v1/ask',  handlers.ask.create);
    app.put('/api/v1/ask/:slug',  jwt({secret: secret.secretToken}), handlers.ask.update);
    app.delete('/api/v1/ask/:slug',  jwt({secret: secret.secretToken}), handlers.ask.remove);

    app.get('/api/v1/complaints',  handlers.complaints.list);
    app.get('/api/v1/complaints/:slug', handlers.complaints.get);
    app.post('/api/v1/complaints',  handlers.complaints.create);
    app.put('/api/v1/complaints/:slug',  jwt({secret: secret.secretToken}), handlers.complaints.update);
    app.delete('/api/v1/complaints/:slug',  jwt({secret: secret.secretToken}), handlers.complaints.remove);

};
