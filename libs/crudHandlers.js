var mongoose = require('mongoose');
var db = require('./mongoose');

module.exports = function (modelName) {

    // Список документов
    var list = function (req, res, next) {
        var filter = {};
        var limit = null;
        var page = (req.query.page > 0 && req.query.page) ? req.query.page - 1 : 0;
        var date = 1;


        if (req.query.category){
            filter.category = req.query.category;
        }
        if (req.query.limit &&  req.query.limit != 'false') {
            limit = req.query.limit;
        }
        if (req.query.date &&  req.query.date != 'false') {
            date = req.query.date;
        }
        if (req.query.tag) {
            filter.tags = req.query.tag;
        }
        if(req.query.search){
            if (req.query.inTitle == 'true') {
                filter.title = {'$regex': req.query.search};
            }
            if (req.query.inTags == 'true') {
                filter.tags = {'$regex': req.query.search};
            }
            if (req.query.inContent == 'true') {
                filter.content = {'$regex': req.query.search};
            }
        }
        console.log('filter', filter);
        if (req.query.random && req.query.random != 'false') {
            console.log('random', filter);

            db.model(modelName)
                .findRandom(filter)
                .skip(page * limit)
                .limit(limit)
                .exec(function(err, data) {
                    db.model(modelName).count(filter).exec(function(err, count) {
                        console.log('count', count);
                        if (err) next(err);
                        res.send({
                            data: data,
                            count: count
                        });
                    })
                });
        } else {
            console.log('not random', filter);

            db.model(modelName)
                .find(filter)
                .skip(page * limit)
                .limit(limit)
                .sort({_id: date})
                .exec(function(err, data) {
                    db.model(modelName).count(filter).exec(function(err, count) {
                        console.log('countcount', count);
                        if (err) next(err);
                        res.send({
                            data: data,
                            count: count
                        });
                    })
                });
        }
    };

    // Один документ
    var get = function (req, res, next) {
        //try{var slug = mongoose.Types.ObjectId(req.params.slug)}
        //catch (e){res.send(400)}
        console.log(req.params.slug);

        db.model(modelName).find({slug: req.params.slug}, function (err, data) {
            if (err) next(err);
            if (data) {
                res.send(data);
            } else {
                res.status(404);
            }
        })
    };

    // Создаем документ
    var create = function (req, res, next) {
        db.model(modelName).create(req.body, function (err, data) {
            if (err) {
                res.status(400).send(err);
            }
            res.send(data);
        });
    };

    // Обновляем документ
    var update = function (req, res, next) {
        //try{var id = mongoose.Types.ObjectId(req.params.id)}
        //catch (e){res.send(400)}

        db.model(modelName).update({slug: req.params.slug}, {$set: req.body}, function (err, numberAffected, data) {
            if (err) next(err);

            if (numberAffected) {
                res.send(200);
            } else {
                res.send(404);
            }

        })
    };

    // Удаляем документ
    var remove = function (req, res, next) {
        //try{var id = mongoose.Types.ObjectId(req.params.id)}
        //catch (e){res.send(400)}
        console.log(req.params.slug);

        db.model(modelName).remove({slug: req.params.slug}, function (err, data) {
            if (err) next(err);
            res.send(data ? req.params.slug : 404);
        });
    };

    return {
        list  : list,
        get   : get,
        create: create,
        update: update,
        remove: remove
    }
};