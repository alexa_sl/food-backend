var express = require('express');
var path = require('path');
var app = express();
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var multer = require('multer');
var db = require('./libs/mongoose'); // Файл работы с базой MongoDB
var log = require('./libs/log')(module);
var RedisStore = require('connect-redis')(session);
var cookieParser = require('cookie-parser');
var routes = require('./routes');
var handlers = {
    recipes: require('./handlers/recipes'),
    questions: require('./handlers/questions'),
    ask: require('./handlers/ask'),
    complaints: require('./handlers/complaints')
};

//CORS middleware
app.all('*', function(req, res, next) {
    // add details of what is allowed in HTTP request headers to the response headers
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Max-Age', '86400');
    res.header('Access-Control-Allow-Headers', 'Authorization,Cache-Control,Origin,Content-Type,Accept');
    res.header('Access-Control-Expose-Headers', 'Origin,Content-Type,Accept,Authorization,X-Set-Authorization');
    //the next() function continues execution and will move onto the requested URL/URI
    next();
});
app.options('*', function(req, res) {
    res.send(200);
});
app.post('/*', function(req, res, next) {
    res.contentType('application/json');
    next();
});

app.use(logger('dev')); // выводим все запросы со статусами в консоль
app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(multer());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    store: new RedisStore,
    resave: true,
    saveUninitialized: true
}));
//app.use(require('prerender-node').set('prerenderServiceUrl', 'http://localhost:3000/').set('prerenderToken', '33nJQo0R5PjVYB0E3GtX'));

function run() {
    routes.setup(app, handlers); // Связуем Handlers с Routes
    db.init(path.join(__dirname, "models"), function (err, data) {
     app.listen(1445, 'localhost');
     //app.listen(8080);
    });
}
run();

app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});
