var mongoose = require('../libs/mongoose');

// Выставляем modelName
var modelName = 'questions';
// Подгружаем стандартные методы для CRUD документов
var handlers = require('../libs/crudHandlers')(modelName);



module.exports = handlers;